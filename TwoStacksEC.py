from lab1 import Stack

class TwoStackQueue(object):
    
    def __init__(self):#creates two stacks to implement one queue.
        self.__first = Stack()
        self.__second = Stack()

    def enqueue(self, newData):

        self.__first.push(newData)#push variable into the first stack.

    def dequeue(self):#first, we fill up one stack, then when we want to dequeu, we pop all the numbers
    # from first to second. Then we pop second (that will be the top of the stacks or the first element
    #added, then we return the values to the first stack)

        while self.__first.isEmpty() == False:
            self.__second.push(self.__first.pop().getData())#push into the second stack the values pop from the first stack.

        temp = self.__second.pop() #Once the transfer is finalize, the first element of the first stack would be the last one inserted in the second one.

        while self.__second.isEmpty() == False:#Once the top is pop, we have to reintroduce all the values from the second stack to the first stack.
            self.__first.push(self.__second.pop().getData())

        return temp

    def printTwoStacks(self):

        self.__first.printStack() #use printStack capabilities from the Stack class

    def isEmpty(self):
        return self.__first.isEmpty() #use isEmpty from the Stack class