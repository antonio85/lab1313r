from sys import argv
from TwoStacksEC import *


class Node(object):#creates nodes to be use by the queue and the stacks
    def __init__(self, data=None, next=None):
        self.__data = data  # __ private data
        self.__next = next
         
    def setData(self, data):
        # Set the "data" data field to the corresponding input
        self.__data = data

    def setNext(self, next):
        # Set the "next" data field to the corresponding input
        self.__next = next

    def getData(self):
        # Return the "data" data field
        return self.__data
            
    def getNext(self):
        # return the "next" data field
        return self.__next


class Queue(object):#first in first out fifo
    def __init__(self):#create head and tail
        self.__head = None
        self.__tail = None
    
    def enqueue(self, newData):
        # Create a new node whose data is newData and whose next node is null
        # Update head and tail
        # Hint: Think about what's different for the first node 
        # added to the Queue
        if self.isEmpty() is True:#if empty, creaes a new head and set tail to behead
            temp = Node(newData, None)
            self.__head = temp
            self.__tail = self.__head
        else:# add the new created node to the head and set tail to the one next to tail.
            temp = Node(newData, None)
            self.__tail.setNext(temp)
            self.__tail = self.__tail.getNext()

    def dequeue(self):
        #  Return the head of the Queue
        #  Update head
        #  Hint: The order you implement the above 2 tasks matters, 
        #  so use a temporary node
        #          to hold important information
        #  Hint: Return null on a empty Queue        
        if self.isEmpty() is True:#empty then return none
            return None

        temp = self.__head
        self.__head = self.__head.getNext()# create temp with head, then head change to the next, and return temp.

        return temp
    
    def isEmpty(self):#check if is empey by checking null
        # Check if the Queue is empty

        if self.__head is None:#checks if head is null, if so returns is true for emptiness.
            return True

        return False
    
    def printQueue(self):
        # Loop through your queue and print each Node's data
        if self.__head is None:
        	print("Empty");

        current = self.__head

        while True:#keep printing until it hits null.
            if current is None:
            	break

            print(current.getData())
            current = current.getNext()



class Stack(object):
    def __init__(self):
        # We want to initialize our Stack to be empty
        # (ie) Set top as null
        self.__top = None

    def push(self, newData):#push data into the top  of the stack2
        # We want to create a node whose data is newData and next node is top
        # Push this new node onto the stack
        # Update top

        newTop = Node(newData, self.__top)#add next to the top. 
        self.__top = newTop # assign the new node to the top.

    def isEmpty(self):
        # Check if the Stack is empty
        if self.__top is None: #if null, returns empty.
            return True

        return False
 
    def pop(self):
        # Return the Node that currently represents the top of the stack
        # Update top
        # Hint: The order you implement the above 2 tasks matters, so use a temporary node
        #         to hold important information
        # Hint: Return null on a empty stack

        if self.isEmpty() is True:#if empty then return null
            return None
        
        temp =  self.__top # save the top into a temp pointer
        self.__top = self.__top.getNext() # move the top to the next node

        return temp  #returns the temp node that hold the old top.
 

        
    def printStack(self):
        # Loop through your stack and print each Node's data
        if self.isEmpty():
        	print("Empty") #just debbugging

        temp = self.__top

        while True:
            if temp is None:
                break

            print(temp.getData())
            temp = temp.getNext()

        
def main(argv):
    # Create a Scanner that reads system input
    
    input_file = argv[1]
    with open(input_file, 'r') as file_ob:
        # Loop over the scanner's input
        # For each line of the input, send it to isPalindrome()
        # If isPalindrome returns true, print "This is a Palindrome." 
        # Otherwise print "Not a Palindrome."
        for line in file_ob:
            #print(line)
            #print(type(line)) figuring out what type line is >> string
            line = line.strip()# remove both ends of the string
            isPalindrome(line)


    return True

def isPalindrome(s):
    # Use your Queue and Stack class to test wheather an input is a palendrome
    myStack = Stack() #creates a stack and a queue
    myQueue = Queue()


    for i in s:#Push the string into the queue and the stack. The stack LIFO mode will invert the number.
        myStack.push(i)
        myQueue.enqueue(i)
    
    i = 1
    while i is not len(s): #it will check if the both data structures return the values in the same order
        q = myQueue.dequeue().getData()
        s = myStack.pop().getData()

        if  int(s) is not int(q):
            print("Not a Palindrome. ")
            return
        
        print("This is a Palindrome.")
        return
'''
def isPalindromeEC(s): #Before splitting into many files.
    # Implement if you wish to do the extra credit.

    myQueue = TwoStackQueue()
    myStack = Stack()

    for i in s:
        myStack.push(i)
        myQueue.enqueue(i)
    
    i = 1
    while i is not len(s): 
        q = myQueue.dequeue().getData()
        s = myStack.pop().getData()

        if  int(s) is not int(q):
            print("Not a Palindrome. ")
            return
        
        print("This is a Palindrome.")
        return
'''

if __name__ == "__main__":
    main(argv)
