
from sys import argv
from TwoStacksEC import *
from lab1 import *


def main(argv):
    # Create a Scanner that reads system input
    
    input_file = argv[1]
    with open(input_file, 'r') as file_ob:
        # Loop over the scanner's input
        # For each line of the input, send it to isPalindrome()
        # If isPalindrome returns true, print "This is a Palindrome." 
        # Otherwise print "Not a Palindrome."
        for line in file_ob:
            #print(line)
            #print(type(line)) figuring out what type line is >> string
            line = line.strip()
            isPalindromeEC(line)


    return True

def isPalindromeEC(s):
    # Implement if you wish to do the extra credit.

    myQueue = TwoStackQueue()# the logic of how to calculate if is a Palindrome is the same than the Non extra credit one.
                             # The only change is that instead of a regular queue, we use the Two Stack queue.
    myStack = Stack()

    for i in s:
        myStack.push(i)
        myQueue.enqueue(i)
    
    i = 1
    while i is not len(s): 
        q = myQueue.dequeue().getData()
        s = myStack.pop().getData()

        if  int(s) is not int(q):
            print("Not a Palindrome. ")
            return
        
        print("This is a Palindrome.")
        return

if __name__ == "__main__":
    main(argv)


